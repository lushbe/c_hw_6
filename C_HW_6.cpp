
#include <iostream>
#include <math.h>


class Homework
{
private:
	int A = 0;
	

public:
	int GetA()
	{
		return A;
	}
	void SetA(int newA)
	{
		A = newA;
	}

};

class Vector 
{
public:

	Vector() : x(0), y(0), z(0)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}

	long double Range()

	{
		long double M = sqrt((x * x) + (y * y) + (z * z));
		return M;
		

	}

private:

	double x;
	double y;
	double z;
	
};



int main()
{
	Homework temp;
	temp.SetA(19);
	std::cout << temp.GetA();

	Vector v(10, 20, 100);
	v.Show();
	std::cout << '\n' << v.Range();

}

